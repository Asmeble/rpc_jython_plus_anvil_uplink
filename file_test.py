## RPC connection between end-points

import anvil.server

anvil.server.connect("<anvil-uplink token goes here>")
# or for client
anvil.server.connect("<anvil-uplink token goes here>-CLIENT")

@anvil.server.callable
def say_hello(name):
  print("Hello from the uplink, %s!" % name)

anvil.server.wait_forever()

