# README #


### What is this repository for? ###
This is for anyone in need of a rpc setup that works with java & python. The project uses jython, java, and anvil-uplink, along with a normal python instance. Feel free to edit the code as you need. The point is to connect
 two end points with uni-directional traffic. If you want communication to be bi-directional, you'll have to use another process to setup the two-way street.